/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * main.cc
 * Copyright (C) 2020 Andreas Wagner <andreas@aw-ubuntu>
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name ``Andreas Wagner'' nor the name of any other
 *    contributor may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * threaded-sockets IS PROVIDED BY Andreas Wagner ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL Andreas Wagner OR ANY OTHER CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <atomic>
#include <string>

#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <pthread.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <aio.h>
#include <stdlib.h>


#define IO_SIGNAL SIGUSR1
#define BUFFER_SIZE (4*1024)

struct thread_init_data
{
	long identifier;
	std::atomic<long> connections;
	pthread_t thread_id;
};

struct io_data
{
	 std::size_t len;
	 void* buf;
	 struct aiocb *aio_data;
	 struct thread_init_data *thread_data;
};

sigset_t null_sigset =
{
	.__val =
	{
		0, 0, 0, 0
	}
};

void action(int, siginfo_t *si, void *)
{
	int len = aio_return(((io_data *)si->si_value.sival_ptr)->aio_data);
	if(len != -1)
	{
		std::string s(
			(char*)(((io_data *)si->si_value.sival_ptr)->aio_data->aio_buf),
			len
			);
		std::cout
			<< ((io_data *)si->si_value.sival_ptr)->thread_data->identifier
			<< ": "
			<< ((io_data *)si->si_value.sival_ptr)->aio_data->aio_nbytes
			<< " -rcv- " << s << std::endl;
		((io_data *)si->si_value.sival_ptr)->thread_data->connections--;
		close(((io_data *)si->si_value.sival_ptr)->aio_data->aio_fildes); // ?
		free((void*)(((io_data *)si->si_value.sival_ptr)->aio_data->aio_buf));
		free(((io_data *)si->si_value.sival_ptr)->aio_data);
		free(((io_data *)si->si_value.sival_ptr));
	}
	else
	{
		std::cerr << "len = -1" << std::endl;
		close(((io_data *)si->si_value.sival_ptr)->aio_data->aio_fildes);
		free((void*)(((io_data *)si->si_value.sival_ptr)->aio_data->aio_buf));
		free(((io_data *)si->si_value.sival_ptr)->aio_data);
		free(((io_data *)si->si_value.sival_ptr));
	}
}

const struct sigaction my_sigaction =
{
#pragma push_macro("sa_sigaction")
#undef sa_sigaction
	.__sigaction_handler =  { .sa_sigaction = &action },
#pragma pop_macro("sa_sigaction")
	.sa_mask  = null_sigset,
	.sa_flags = SA_SIGINFO
};


struct msg_type
{
	long int mtype;
	char control;
	int fd;
};



volatile int msg_d = -1;

void *thread(void *data)
{

	
	bool go_on = true;
	struct msg_type msg;
	while (go_on)
	{
		ssize_t msg_size
			= msgrcv(
			         msg_d,
			         &msg,
			         sizeof(msg_type)-sizeof(long int),
			         ((struct thread_init_data*)data)->identifier, // msgtype
			         0);
		if(msg_size < 0)
		{
			if(errno != EINTR)
			{
				std::cerr << strerror(errno) << std::endl;
				std::cerr << "msg in thread()" << std::endl;
			}
		}
		else
		{
			((struct thread_init_data*)data)->connections++;

			struct aiocb *test = (struct aiocb*) malloc(sizeof(struct aiocb));
			struct io_data *my_io_data =
				(struct io_data*) malloc(sizeof(struct io_data));
			char *buf = (char*) malloc(BUFFER_SIZE);
			memset((void*)test, 0, sizeof(test));

			test->aio_fildes = msg.fd;
			test->aio_offset = 0;
			test->aio_buf = (void*) buf;
			test->aio_nbytes = BUFFER_SIZE;
			test->aio_reqprio = 0;
			test->aio_sigevent.sigev_notify = SIGEV_SIGNAL;
			test->aio_sigevent.sigev_signo = IO_SIGNAL;
			test->aio_sigevent.sigev_value.sival_ptr = my_io_data;

			my_io_data->len = BUFFER_SIZE;
			my_io_data->buf = (void*) buf;
			my_io_data->aio_data = test;
			my_io_data->thread_data = (thread_init_data*) data;

			aio_read(test);
		}
	}
	return NULL;
}


struct thread_init_data *t_init_data;
pthread_attr_t *thread_attr;
int numthreads = 4;
int listen_socket_fd;
int max_conns_per_thread = 2; //TODO

void sigint_handler(int)
{
	for(int i = 0; i < numthreads; i++)
	{
		pthread_kill(t_init_data[i].thread_id, SIGKILL);
	}
	free(t_init_data);
	free(thread_attr);
	close(listen_socket_fd);
	exit(0);
}


int main()
{
	msg_d = msgget(IPC_PRIVATE, IPC_CREAT | 0666);


	t_init_data = (thread_init_data*) malloc(sizeof(thread_init_data) * numthreads);
	thread_attr = (pthread_attr_t *) malloc(sizeof(pthread_attr_t) * numthreads);
	for(int i = 0; i < numthreads; i++)
	{
		t_init_data[i].identifier = i+1;
		t_init_data[i].connections = 0;
		if(pthread_attr_init(&thread_attr[i]) != 0)
		{
			std::cerr << "pthread_attr_init()!" << std::endl;
		}
		pthread_create(&t_init_data[i].thread_id,
			           &thread_attr[i],
			           &thread,
			           (void*)&t_init_data[i]);
	}
	
	signal(SIGINT, &sigint_handler);
	
	const struct sockaddr_in6 sockaddr =
	{
		.sin6_family = AF_INET6,
		.sin6_port = htons(8080),
		.sin6_flowinfo = 0, // ?
		.sin6_addr = in6addr_any,
		.sin6_scope_id = 0 // ?
	};
	listen_socket_fd = socket(AF_INET6, SOCK_STREAM, 0);
	const int do_use_so_reuseaddr = 1;
	setsockopt(listen_socket_fd,
	           SOL_SOCKET,
	           SO_REUSEADDR,
	           &do_use_so_reuseaddr,
	           sizeof(int));
	std::cout << "socket" << std::endl;
	if(bind(listen_socket_fd,
	        (struct sockaddr*) &sockaddr,
	        sizeof(sockaddr)) != 0)
	{
		std::cerr << "bind! " << strerror(errno) << std::endl;
		exit(1);
	}
	std::cout << "bound" << std::endl;
	if(listen(listen_socket_fd, 64) != 0)
	{
		std::cerr << "listen!" << std::endl;
		exit(1);
	}
	std::cout << "listening" << std::endl;

	sigaction(IO_SIGNAL, &my_sigaction, NULL);
	
	std::uint8_t count = 0; 
	bool go_on = true;
	while(go_on)
	{
		struct sockaddr_in6 remote_addr;
		socklen_t addr_len = sizeof(remote_addr);
		int remote_fd = 0;
		std::cout << "going to accept" << std::endl;
		if((remote_fd = accept(listen_socket_fd,
		                       (struct sockaddr*) &remote_addr,
		                       &addr_len)
		    ) >= 0)
		{	
			std::cout << "accepted" << std::endl;
			for(int i = 0; i < 16; i++)
			{
				std::cout << (int) remote_addr.sin6_addr.s6_addr[i]
					<< ":";
			}
			std::cout << std::endl;

			bool free_found = false;
			for(int i = 0; i < numthreads; i++)
			{
				if(t_init_data[(count+i) % numthreads].connections < max_conns_per_thread)
				{
					free_found = true;
					count += i;
					break;
				}
			}
			if(!free_found)
			{
				close(remote_fd);
				std::cout << "rejected" << std::endl;
				continue;
			}
			msg_type msg =
			{
				.mtype = (count % numthreads)+1,
				.control = '\0',
				.fd = remote_fd
			};
			count++;
			bool retry = true;
			while(retry)
			{
				if(
				   msgsnd(
				           msg_d,
				           &msg,
				           sizeof(msg)-sizeof(long int),
				           0
				           )
				   < 0)
				{
					if(errno != EAGAIN)
					{
						retry = false;
						std::cerr << "msgsend()!" << std::endl;
					}
				}
				else
				{
					retry = false;
				}
			}
			
		}
		else // accept() returned negative number
		{
			if(errno != EINTR)
				std::cerr << "accept! " << strerror(errno) << std::endl;
		}
		
	}

	close(listen_socket_fd);
	std::cout << "Hello world!" << std::endl;
	//pthread_join(thread_id);
	return 0;
}

